import api from './index'
import { axios } from '@/utils/request'

/**
 *
 * 登录bug
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */

// 用户名密码登录 http://localhost:8002/auth/oauth/token?username=admin&grant_type=password&scope=select&client_id=prex-app&client_secret=123456&password=admin
export function login (username, password, code, t) {
  const grant_type = 'password'
  const scope = 'server'
  return axios({
    url: '/auth/oauth/token',
    headers: {
      'Authorization': 'Basic cHJleDoxMjM0NTY='
    },
    method: 'post',
    params: { username, password, t, code, grant_type, scope }
  })
}

// 根据手机号登陆
export function loginByUserPhone (phone, smsCode) {
  const data = {
    phone,
    smsCode
  }
  // 登录
  return axios({
    url: '/auth/mobile/login',
    headers: {
      'Authorization': 'Basic cHJleDoxMjM0NTY='
    },
    method: 'post',
    params: data
  })
}

export function loginBySocial (state, code) {
  const grant_type = 'mobile'

  const data = {
    state,
    code
  }
  return axios({
    url: '/auth/social',
    headers: {
      'Authorization': 'Basic cHJleDoxMjM0NTY='
    },
    method: 'post',
    params: data
  })
}

// export function login (parameter) {
//   return axios({
//     url: '/auth/login',
//     method: 'post',
//     data: parameter
//   })
// }

export function getSmsCode (phone) {
  return axios({
    url: '/auth/sendCode/' + phone,
    headers: {
      'Authorization': 'Basic cHJleDoxMjM0NTY='
    },
    method: 'post'
  })
}

export function getInfo () {
  return axios({
    url: '/sys/info',
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function logout () {
  return axios({
    url: '/sys/logout',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
